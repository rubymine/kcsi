Rails.application.routes.draw do
  get 'home/index'
  get 'home/about'
  devise_for :penggunas
  resources :tulisans do
    resources :comments
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
