require "application_system_test_case"

class TulisansTest < ApplicationSystemTestCase
  setup do
    @tulisan = tulisans(:one)
  end

  test "visiting the index" do
    visit tulisans_url
    assert_selector "h1", text: "Tulisans"
  end

  test "creating a Tulisan" do
    visit tulisans_url
    click_on "New Tulisan"

    fill_in "Isi", with: @tulisan.isi
    fill_in "Judul", with: @tulisan.judul
    fill_in "Penulis", with: @tulisan.penulis
    click_on "Create Tulisan"

    assert_text "Tulisan was successfully created"
    click_on "Back"
  end

  test "updating a Tulisan" do
    visit tulisans_url
    click_on "Edit", match: :first

    fill_in "Isi", with: @tulisan.isi
    fill_in "Judul", with: @tulisan.judul
    fill_in "Penulis", with: @tulisan.penulis
    click_on "Update Tulisan"

    assert_text "Tulisan was successfully updated"
    click_on "Back"
  end

  test "destroying a Tulisan" do
    visit tulisans_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Tulisan was successfully destroyed"
  end
end
