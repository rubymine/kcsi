require 'test_helper'

class TulisansControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tulisan = tulisans(:one)
  end

  test "should get index" do
    get tulisans_url
    assert_response :success
  end

  test "should get new" do
    get new_tulisan_url
    assert_response :success
  end

  test "should create tulisan" do
    assert_difference('Tulisan.count') do
      post tulisans_url, params: { tulisan: { isi: @tulisan.isi, judul: @tulisan.judul, penulis: @tulisan.penulis } }
    end

    assert_redirected_to tulisan_url(Tulisan.last)
  end

  test "should show tulisan" do
    get tulisan_url(@tulisan)
    assert_response :success
  end

  test "should get edit" do
    get edit_tulisan_url(@tulisan)
    assert_response :success
  end

  test "should update tulisan" do
    patch tulisan_url(@tulisan), params: { tulisan: { isi: @tulisan.isi, judul: @tulisan.judul, penulis: @tulisan.penulis } }
    assert_redirected_to tulisan_url(@tulisan)
  end

  test "should destroy tulisan" do
    assert_difference('Tulisan.count', -1) do
      delete tulisan_url(@tulisan)
    end

    assert_redirected_to tulisans_url
  end
end
