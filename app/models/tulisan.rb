class Tulisan < ApplicationRecord
  paginates_per 10
  belongs_to :pengguna
  has_many :comments 
end
