json.extract! tulisan, :id, :judul, :isi, :penulis, :created_at, :updated_at
json.url tulisan_url(tulisan, format: :json)
