class CommentsController < ApplicationController
  def create
    @tulisan = Tulisan.find(params[:tulisan_id])
    @comment = @tulisan.comments.create(params[:comment].permit(:comment))
    @comment.pengguna_id = current_pengguna.id if current_pengguna
    @comment.save

    if @comment.save
      redirect_to tulisan_path(@tulisan)
    else
      render 'new'
    end
  end

  def destroy
    @tulisan = Tulisan.find(params[:tulisan_id])
    @comment = @tulisan.comments.find(params[:id])
    @comment.destroy
    redirect_to tulisan_path(@tulisan)
  end

  def edit
    @tulisan = Tulisan.find(params[:tulisan_id])
    @comment = @tulisan.comments.find(params[:id])
  end

  def update
    @tulisan = Tulisan.find(params[:comment].permit(:comment))
    @comment = @tulisan.comments.find(params[:id])
    if @comment.update(params[:comment].permit(:comment))
      redirect_to tulisan_path(@tulisan)
    else
      render 'edit'
    end
  end
end
