class TulisansController < ApplicationController
  before_action :set_tulisan, only: [:show, :edit, :update, :destroy]

  # GET /tulisans
  # GET /tulisans.json
  def index
    @tulisans = Tulisan.order(:judul).page params[:page]
    #= User.order(:name).page params[:page]
  end

  # GET /tulisans/1
  # GET /tulisans/1.json
  def show
  end

  # GET /tulisans/new
  def new
    @tulisan = current_pengguna.tulisans.build
  end

  # GET /tulisans/1/edit
  def edit
  end

  # POST /tulisans
  # POST /tulisans.json
  def create
    @tulisan = current_pengguna.tulisans.build(tulisan_params)

    respond_to do |format|
      if @tulisan.save
        format.html { redirect_to @tulisan, notice: 'Tulisan was successfully created.' }
        format.json { render :show, status: :created, location: @tulisan }
      else
        format.html { render :new }
        format.json { render json: @tulisan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tulisans/1
  # PATCH/PUT /tulisans/1.json
  def update
    respond_to do |format|
      if @tulisan.update(tulisan_params)
        format.html { redirect_to @tulisan, notice: 'Tulisan was successfully updated.' }
        format.json { render :show, status: :ok, location: @tulisan }
      else
        format.html { render :edit }
        format.json { render json: @tulisan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tulisans/1
  # DELETE /tulisans/1.json
  def destroy
    @tulisan.destroy
    respond_to do |format|
      format.html { redirect_to tulisans_url, notice: 'Tulisan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tulisan
      @tulisan = Tulisan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tulisan_params
      params.require(:tulisan).permit(:judul, :isi, :penulis)
    end
end
