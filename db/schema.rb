# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_13_132715) do

  create_table "comments", force: :cascade do |t|
    t.text "comment"
    t.integer "pengguna_id"
    t.integer "tulisan_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pengguna_id"], name: "index_comments_on_pengguna_id"
    t.index ["tulisan_id"], name: "index_comments_on_tulisan_id"
  end

  create_table "penggunas", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_penggunas_on_email", unique: true
    t.index ["reset_password_token"], name: "index_penggunas_on_reset_password_token", unique: true
  end

  create_table "tulisans", force: :cascade do |t|
    t.text "judul"
    t.text "isi"
    t.text "penulis"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "pengguna_id"
  end

end
