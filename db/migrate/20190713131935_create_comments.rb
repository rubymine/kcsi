class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.text :comment
      t.references :pengguna, foreign_key: true
      t.references :tulisan, foreign_key: true

      t.timestamps
    end
  end
end
