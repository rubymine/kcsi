class CreateTulisans < ActiveRecord::Migration[5.2]
  def change
    create_table :tulisans do |t|
      t.text :judul
      t.text :isi
      t.text :penulis

      t.timestamps
    end
  end
end
